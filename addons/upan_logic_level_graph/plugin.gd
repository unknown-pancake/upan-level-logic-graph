tool
extends EditorPlugin

const LevelGraph = preload("./level_graph.tscn")


var _level_graph



func _enter_tree() -> void:
	_level_graph = LevelGraph.instance()
	_level_graph.undoredo = get_undo_redo()
	_level_graph.connect("level_logic_node_selected", self, "_on_lln_selected")
	_level_graph.connect("refresh_requested", self, "_on_refresh_requested")
	add_control_to_bottom_panel(_level_graph, "Level Graph")
	
	connect("scene_changed", self, "_on_scene_changed")
	
	var selection = get_editor_interface().get_selection()
	selection.connect("selection_changed", self, "_on_selection_changed")

func _exit_tree() -> void:
	remove_control_from_bottom_panel(_level_graph)
	_level_graph.free()




func _on_scene_changed(root: Node) -> void:
	_level_graph.on_scene_changed(root)

func _on_selection_changed() -> void:
	var selection = get_editor_interface().get_selection()
	var selected_nodes = selection.get_selected_nodes()
	
	if selected_nodes.size() != 1:
		return
	
	_level_graph.on_node_selected(selected_nodes[0])

func _on_lln_selected(node: Node) -> void:
	var selection = get_editor_interface().get_selection()
	selection.clear()
	selection.add_node(node)

func _on_refresh_requested() -> void:
	_level_graph.on_scene_changed(get_editor_interface().get_edited_scene_root())
