tool
extends GraphEdit


signal level_logic_node_selected(scene_node)
signal refresh_requested()



const LevelLogicNode = preload("./level_logic_node.tscn")



var to_scene_map: Dictionary = {}
var from_scene_map: Dictionary = {}

var undoredo: UndoRedo

var sync_selection: bool = true
var view_props: bool = true

var scene_root: Node



func _enter_tree() -> void:
	add_valid_connection_type(0, 0)



func on_node_selected(node: Node) -> void:
	if not from_scene_map.has(node) or not sync_selection:
		return
	
	var lln: GraphNode = from_scene_map[node]
	set_selected(lln)
	
#	var new_scroll = lln.offset - rect_size / 2 + lln.rect_size / 2
#	scroll_offset = new_scroll



func on_scene_changed(root: Node) -> void:
	scene_root = root
	
	clear_connections()
	clear_nodes()
	clear_maps()
	
	if root == null:
		$rclick.set_item_disabled(2, true)
		$rclick.set_item_disabled(3, true)
		return
	
	$rclick.set_item_disabled(2, false)
	$rclick.set_item_disabled(3, false)
	
	check_upan_data(root)
	check_upan_metadata(root)
	scan_scene(root)
	
	if to_scene_map.size() == 0:
		root.get_node("upan_data").queue_free()



func clear_nodes() -> void:
	for child in get_children():
		if child is GraphNode:
			child.queue_free()

func clear_maps() -> void:
	to_scene_map.clear()
	from_scene_map.clear()

func check_upan_data(root: Node) -> void:
	if not root.has_node("upan_data"):
		var node = Node.new()
		root.add_child(node)
		
		node.name = "upan_data"
		node.owner = root
		root.move_child(node, 0)

func check_upan_metadata(root: Node) -> void:
	var upan_data = root.get_node("upan_data")
	
	for meta_name in upan_data.get_meta_list():
		if not meta_name.begins_with("_upan"):
			continue
		
		var path_start_idx = meta_name.find("{")
		if path_start_idx == -1:
			continue
		
		var path = meta_name.substr(path_start_idx+1)
		path = path.left(path.length() - 1)
		
		if not root.has_node(path):
			upan_data.remove_meta(meta_name)

func scan_scene(root: Node) -> void:
	create_logic_nodes(root)
	create_connections(root)

func create_logic_nodes(root: Node) -> void:
	for node in get_tree().get_nodes_in_group("level_logic"):
		if node.owner == root:
			var lln = LevelLogicNode.instance()
			lln.undoredo = undoredo
			lln.scene_node = node
			lln.show_props = view_props
			add_child(lln)
			
			to_scene_map[lln] = node
			from_scene_map[node] = lln

func create_connections(root: Node) -> void:
	for src_lln in to_scene_map.keys():
		var node: Node = src_lln.scene_node
		
		for sig in node.get_signal_list():
			if not sig.name.begins_with("trigger_"):
				continue
			
			var src_slot_id = src_lln.out_slot_map[sig.name]
			
			for conn in node.get_signal_connection_list(sig.name):
				if not from_scene_map.has(conn.target):
					continue
				if not conn.method.begins_with("action_"):
					continue
				
				var dst_lln = from_scene_map[conn.target]
				var dst_slot_id = dst_lln.in_slot_map[conn.method]
				
				connect_node(src_lln.name, src_slot_id, dst_lln.name, dst_slot_id)



func _on_connection_request(from: String, from_slot: int, to: String, to_slot: int) -> void:
	var src_lln = get_node(from)
	var dst_lln = get_node(to)
	var src_signal = src_lln.out_slot_map[from_slot]
	var dst_meth = dst_lln.in_slot_map[to_slot]
	
	undoredo.create_action("Level Logic Node Connected", UndoRedo.MERGE_DISABLE)
	undoredo.add_do_method(src_lln.scene_node, "connect", src_signal, dst_lln.scene_node, dst_meth, [], CONNECT_PERSIST)
	undoredo.add_do_method(self, "connect_node", from, from_slot, to, to_slot)
	undoredo.add_undo_method(src_lln.scene_node, "disconnect", src_signal, dst_lln.scene_node, dst_meth)
	undoredo.add_undo_method(self, "disconnect_node", from, from_slot, to, to_slot)
	undoredo.commit_action()

func _on_disconnection_request(from: String, from_slot: int, to: String, to_slot: int) -> void:
	var src_lln = get_node(from)
	var dst_lln = get_node(to)
	var src_signal = src_lln.out_slot_map[from_slot]
	var dst_meth = dst_lln.in_slot_map[to_slot]
	
	undoredo.create_action("Level Logic Node Disconnected", UndoRedo.MERGE_DISABLE)
	undoredo.add_do_method(src_lln.scene_node, "disconnect", src_signal, dst_lln.scene_node, dst_meth)
	undoredo.add_do_method(self, "disconnect_node", from, from_slot, to, to_slot)
	undoredo.add_undo_method(src_lln.scene_node, "connect", src_signal, dst_lln.scene_node, dst_meth, [], CONNECT_PERSIST)
	undoredo.add_undo_method(self, "connect_node", from, from_slot, to, to_slot)
	undoredo.commit_action()

func _on_node_selected(node: Node) -> void:
	if not sync_selection:
		return
	
	var scene_node = to_scene_map[node]
	emit_signal("level_logic_node_selected", scene_node)

func _on_level_graph_popup_request(position: Vector2) -> void:
	$rclick.rect_position = position
	$rclick.popup()



var ReadyTrigger = preload("./builtins/ready/ready.tscn")
var AllTrigger = preload("./builtins/all/all.tscn")


func spawn_node(scene: PackedScene) -> void:
	check_upan_data(scene_root)
	var upan_data = scene_root.get_node("upan_data")
	var node = scene.instance(PackedScene.GEN_EDIT_STATE_INSTANCE)
	
	upan_data.add_child(node, true)
	node.owner = scene_root
	
	emit_signal("refresh_requested")


func _on_rclick_index_pressed(index: int) -> void:
	match index:
		0: # refresh
			emit_signal("refresh_requested")
			
		2: # synchronize selection
			var checked = $rclick.is_item_checked(2)
			$rclick.set_item_checked(2, not checked)
			
			sync_selection = not checked
		3: # show properties
			var checked = $rclick.is_item_checked(3)
			$rclick.set_item_checked(3, not checked)
			
			view_props = not checked
			emit_signal("refresh_requested")
			
		5: # new ready trigger
			spawn_node(ReadyTrigger)
		6: # new all trigger
			spawn_node(AllTrigger)
