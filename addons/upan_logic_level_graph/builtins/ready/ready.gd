extends Node


signal trigger_ready(data)

export var prop_instantaneous: bool = false


func _ready() -> void:
	call_deferred("emit_signal", "trigger_ready", {
		'instantaneous': prop_instantaneous
	})

