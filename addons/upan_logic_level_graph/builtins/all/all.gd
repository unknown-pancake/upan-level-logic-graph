extends Node



signal trigger_all_triggered(data)



var counter = 0



func _ready() -> void:
	for iconn in get_incoming_connections():
		if iconn.method_name == "action_trigger":
			counter += 1



func action_trigger(data: Dictionary) -> void:
	counter -= 1
	
	if counter == 0:
		emit_signal("trigger_all_triggered", data)
