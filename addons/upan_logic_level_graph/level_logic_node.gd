tool
extends GraphNode


var scene_node: Node

var in_slot_map := {}
var out_slot_map := {}

var undoredo: UndoRedo
var show_props: bool

var _in_replace := false



func _enter_tree() -> void:
	scene_node.connect("renamed", self, "_update_title")
	
	if scene_node.get_parent().name == "upan_data":
		show_close = true
	
	_update_title()
	if show_props:
		_create_prop_slots()
	_create_output_slots()
	_create_input_slots()
	_replace()



func _update_title() -> void:
	title = scene_node.name

func _create_prop_slots() -> void:
	for prop in scene_node.get_property_list():
		if prop.name.begins_with("prop_"):
			match prop.type:
				TYPE_BOOL:
					var chkbox = CheckBox.new()
					chkbox.text = prop.name.trim_prefix("prop_")
					chkbox.align = Button.ALIGN_CENTER
					chkbox.pressed = scene_node.get(prop.name)
					chkbox.connect("toggled", self, "_on_prop_set", [prop.name])
					
					add_child(chkbox)
				
				TYPE_INT:
					match prop.hint:
						PROPERTY_HINT_NONE:
							var hb = HBoxContainer.new()
							var lbl = Label.new()
							var sbox = SpinBox.new()
							
							hb.add_child(lbl)
							hb.add_child(sbox)
							
							lbl.text = prop.name.trim_prefix("prop_")
							
							sbox.min_value = -9999999
							sbox.max_value = 9999999
							sbox.step = 1
							sbox.value = scene_node.get(prop.name)
							sbox.connect("value_changed", self, "_on_prop_set", [prop.name])
							
							add_child(hb)
						
						PROPERTY_HINT_ENUM:
							var hb = HBoxContainer.new()
							var lbl = Label.new()
							var obut = OptionButton.new()
							
							hb.add_child(lbl)
							hb.add_child(obut)
							
							lbl.text = prop.name.trim_prefix("prop_")
							
							for item in prop.hint_string.split(","):
								obut.add_item(item)
								
							obut.selected = scene_node.get(prop.name)
							obut.connect("item_selected", self, "_on_prop_set", [prop.name])
							
							add_child(hb)

func _create_output_slots() -> void:
	for sig in scene_node.get_signal_list():
		if sig.name.begins_with("trigger_"):
			var lbl = Label.new()
			var id = get_child_count()
			
			lbl.text = sig.name.trim_prefix("trigger_")
			lbl.align = Label.ALIGN_RIGHT
			
			add_child(lbl)
			set_slot(id, false, 0, Color.purple, true, 0, Color.purple)
			
			id = out_slot_map.size() / 2
			out_slot_map[sig.name] = id
			out_slot_map[id] = sig.name
			

func _create_input_slots() -> void:
	for meth in scene_node.get_method_list():
		if meth.name.begins_with("action_"):
			var lbl = Label.new()
			var id = get_child_count()
			
			lbl.text = meth.name.trim_prefix("action_")
			lbl.align = Label.ALIGN_LEFT
			
			add_child(lbl)
			set_slot(id, true, 0, Color.purple, false, 0, Color.purple)
			
			id = in_slot_map.size() / 2
			in_slot_map[meth.name] = id
			in_slot_map[id] = meth.name

func _replace() -> void:
	var upan_data = scene_node.owner.get_node("upan_data")
	var path = scene_node.owner.get_path_to(scene_node)
	var key = "{" + str(path) + "}"
	
	_in_replace = true
	
	if upan_data.has_meta("_upan_llg_position" + key):
		offset = upan_data.get_meta("_upan_llg_position" + key)
	if upan_data.has_meta("_upan_llg_size" + key):
		rect_size = upan_data.get_meta("_upan_llg_size" + key)
	
	_in_replace = false



func _save_position() -> void:
	if _in_replace:
		return
	
	var upan_data = scene_node.owner.get_node("upan_data")
	var path = scene_node.owner.get_path_to(scene_node)
	var key = "{" + str(path) + "}"
	
	var old_offset = upan_data.get_meta("_upan_llg_position" + key)
	
	upan_data.set_meta("_upan_llg_position" + key, offset)
	
	if undoredo.is_commiting_action():
		return
	
	undoredo.create_action("Moved Level Logic Node " + key, UndoRedo.MERGE_ENDS)
	undoredo.add_do_method(upan_data, "set_meta", "_upan_llg_position" + key, offset)
	undoredo.add_undo_property(self, "offset", old_offset)
	undoredo.commit_action()

func _update_size(new_size: Vector2) -> void:
	if _in_replace:
		rect_size = new_size
		return
	
	var upan_data = scene_node.owner.get_node("upan_data")
	var path = scene_node.owner.get_path_to(scene_node)
	var key = "{" + str(path) + "}"
	
	undoredo.create_action("Resized Level Logic Node " + key, UndoRedo.MERGE_ENDS)
	undoredo.add_do_property(self, "rect_size", new_size)
	undoredo.add_do_method(upan_data, "set_meta", "_upan_llg_size" + key, new_size)
	undoredo.add_undo_property(self, "rect_size", rect_size)
	undoredo.add_undo_method(upan_data, "set_meta", "_upan_llg_size" + key, rect_size)
	undoredo.commit_action()



func _on_prop_set(val, prop: String) -> void:
	var old_value = scene_node.get(prop)
	
	undoredo.create_action("Logic Level Node Property Set")
	undoredo.add_do_property(scene_node, prop, val)
	undoredo.add_undo_property(scene_node, prop, val)
	undoredo.commit_action()



func _on_level_logic_node_close_request() -> void:
	scene_node.queue_free()
	queue_free()
